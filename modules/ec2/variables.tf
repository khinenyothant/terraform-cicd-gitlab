# Region
variable "region" {}

# AMI ID
variable "ami_id" {}

# Instance Type
variable "instance_type" {}

# Instance Name
variable "instance_name" {}