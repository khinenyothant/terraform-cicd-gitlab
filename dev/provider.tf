provider "aws" {
  region = var.region
}

terraform {
    backend "s3" {
        bucket         = "terraform-state-backet-gitlab"
        key            = "dev/terraform.tfstate"
        region         = "ap-northeast-1"
    }
}